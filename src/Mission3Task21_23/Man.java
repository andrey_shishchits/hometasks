package Mission3Task21_23;

/**
 * Created by User-PC on 13.05.2017.
 */

/**
 * This class contains information about man.
 */
public class Man {
    private String firstName;
    private String lastName;
    private int phoneNumber;

    /**
     * Creates object of class with initialized fields(see the parameters).
     *
     * @param firstName
     * @param phoneNumber
     */
    public Man(String firstName, int phoneNumber) {
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
    }

    /**
     * Creates object of class with initialized fields(see the parameters).
     *
     * @param firstName
     * @param lastName
     * @param phoneNumber
     */
    public Man(String firstName, String lastName, int phoneNumber) {
        this(firstName, lastName);
        this.phoneNumber = phoneNumber;
    }

    /**
     * Creates object of class with initialized fields(see the parameters).
     *
     * @param firstName
     * @param lastName
     */
    public Man(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Man{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber=" + phoneNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Man)) return false;

        Man man = (Man) o;

        return phoneNumber == man.phoneNumber;
    }
}
