package Mission3Task16;

import Mission3Task17.Cat;

/**
 * Created by User-PC on 13.05.2017.
 */

/**
 * This class wraps Cat class.
 * @author Andrey Shishchits
 */
public final class Zoo {
    private String zooName;
    private Cat cat;

    /**
     * This constructor initializes new object with setting values.
     *
     * @param zooName - Zoo name
     * @param catName - Cat name
     * @param catAge - Cat age
     */
    public Zoo(String zooName, String catName, int catAge) {
        this.zooName = zooName;
        cat = new Cat(catName);
        cat.setAge(catAge);
    }

    @Override
    public String toString() {
        String s1 = "There is " + cat.toString() + " in " + zooName + " zoo.";

        return s1;
    }
}
