package Mission3Task16;

import Mission1Task4.UserInput;

/**
 * Created by User-PC on 13.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        String zooName = UserInput.inputAction("Enter zoo name: ");
        String catName = UserInput.inputAction("Enter cat name: ");
        int catAge = UserInput.inputInt("Enter the cat age: ");
        Zoo zoo = new Zoo(zooName, catName, catAge);

        System.out.println(zoo.toString());
    }

}
