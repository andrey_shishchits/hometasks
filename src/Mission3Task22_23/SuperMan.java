package Mission3Task22_23;

import Mission3Task21_23.Man;

/**
 * Created by User-PC on 15.05.2017.
 */

/**
 * This class contains information about superman.
 */
public class SuperMan extends Man {
    private String superPower;

    /**
     * Creates object of class with initialized fields(see the parameters).
     *
     * @param firstName
     * @param lastName
     * @param superPower
     */
    public SuperMan(String firstName, String lastName, String superPower) {
        super(firstName, lastName);
        this.superPower = superPower;
    }

    /**
     * Creates object of class with initialized fields(see the parameters).
     *
     * @param firstName
     * @param lastName
     * @param superPower
     * @param phoneNumber
     */
    public SuperMan(String firstName, String lastName, String superPower, int phoneNumber) {
        super(firstName, lastName, phoneNumber);
        this.superPower = superPower;
    }

    @Override
    public String toString() {

        return super.toString() + "SuperMan{" +
                "superPower='" + superPower + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SuperMan)) return false;

        SuperMan superMan = (SuperMan) o;

        return superPower == superMan.superPower;
    }
}
