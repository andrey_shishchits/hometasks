package Mission2Task15;

import Mission1Task4.UserInput;

/**
 * Created by User-PC on 04.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        String str;
        str = UserInput.inputAction("Enter string: ");

        System.out.println("Enter 1 if u want to delete all numbers from string: ");
        System.out.println("Enter 2 if u want to delete all russian letters from string: ");
        System.out.println("Enter 3 if u want to leave russians letters & numbers only: ");

        int action = 0;

        action = UserInput.inputInt("Chose action: ");

        switch (action) {
            case 1:
                System.out.println(StringChanger.dismissNumbers(str));
                break;
            case 2:
                System.out.println(StringChanger.dismissRussianLetters(str));
                break;
            case 3:
                System.out.println(StringChanger.dimissAllExeptLettersAndNumbers(str));
                break;
        }

    }
}
