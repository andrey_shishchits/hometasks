package Mission2Task15;

/**
 * Created by User-PC on 04.05.2017.
 */

/**
 * This utility class contains several methods which developed for string converting.
 */
public final class StringChanger {

    private StringChanger() {

    }

    /**
     * This method excludes numbers from string.
     * @param str
     * @return
     */
    public static String dismissNumbers(String str) {
        return str.replaceAll("\\d", "");
    }

    /**
     * This method excludes all letters of russian alphabet and replaces them with question marks in string.
     * @param str
     * @return
     */
    public static String dismissRussianLetters(String str) {
        return str.replaceAll("[А-Яа-яЁё]", "?");
    }

    /**
     * This method excludes everything except letters and numbers from string.
     * @param str
     * @return
     */
    public static String dimissAllExeptLettersAndNumbers(String str) {
        return str.replaceAll("[^A-Za-zА-Яа-яЁё0-9]", "");
    }
}
