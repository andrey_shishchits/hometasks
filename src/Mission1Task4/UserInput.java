package Mission1Task4;

/**
 * Created by User-PC on 26.04.2017.
 */

import java.util.Scanner;

/**
 * Subclass for user input.
 */
public class UserInput {
    /**
     * This method receive float number from Console.
     * And also prints input message.
     * @param msg
     * @return number
     */
    public static float inputNumber(String msg) {
        Scanner scanner = new Scanner(System.in);

        System.out.print(msg);
        float number = scanner.nextFloat();

        return number;
    }

    /**
     * This method receive String varaible from Console.
     * And also prints input message.
     * @param msg
     * @return action
     */
    public static String inputAction(String msg) {
        Scanner scanner = new Scanner(System.in);

        System.out.print(msg);
        String action = scanner.nextLine();

        return action;
    }

    /**
     * This method recieve int value from Console.
     * And also prints input message.
     * @param msg
     * @return
     */
    public static int inputInt(String msg) {
        Scanner scanner = new Scanner(System.in);

        System.out.print(msg);
        int number = scanner.nextInt();

        return number;
    }
}

