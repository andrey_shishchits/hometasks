package Mission1Task4;

/**
 * Created by User-PC on 26.04.2017.
 */

/**
 * Model
 * Class contains main logic of Calculation program
 */
public class Calculator {

    private float firtsNumber = 0;
    private float secondNumber = 0;
    private float result = 0;

    /**
     * Division method
     */
    public void division() {
        result = firtsNumber / secondNumber;
    }

    /**
     * Sum method
     */
    public void sum() {
        result = firtsNumber + secondNumber;
    }

    /**
     * Multiplication method
     */
    public void multiplication() {
        result = firtsNumber * secondNumber;
    }

    /**
     * Difference method
     */
    public void difference() {
        result = firtsNumber - secondNumber;
    }


    //geteer/setter fold
    public float getFirtsNumber() {
        return firtsNumber;
    }

    public float getSecondNumber() {
        return secondNumber;
    }

    public float getResult() {
        return result;
    }

    public void setFirtsNumber(float firtsNumber) {
        this.firtsNumber = firtsNumber;
    }

    public void setSecondNumber(float secondNumber) {
        this.secondNumber = secondNumber;
    }

}
