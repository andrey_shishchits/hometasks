package Mission1Task4;

/**
 * Created by User-PC on 26.04.2017.
 */

/**
 * Controller
 */
public class Main {

    public static void main(String[] args) {

        Calculator calculate = new Calculator();

        calculate.setFirtsNumber(UserInput.inputNumber("Enter first number: "));
        calculate.setSecondNumber(UserInput.inputNumber("Enter second number: "));

        String action = UserInput.inputAction("Enter action: ");

        final String plus = "+";
        final String minus = "-";
        final String division = "/";
        final String multi = "*";


        switch (action) {

            case plus:
                calculate.sum();
                break;
            case minus:
                calculate.difference();
                break;
            case division:
                calculate.division();
                break;
            case multi:
                calculate.multiplication();
                break;
        }

        View.print("Result " + action + ": " + calculate.getResult());

    }
}
