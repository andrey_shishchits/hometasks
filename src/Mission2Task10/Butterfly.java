package Mission2Task10;

/**
 * Created by User-PC on 04.05.2017.
 */

/**
 * This class prints array in the form of butterfly.
 */
public class Butterfly {
    private int[][] arrayButterfly;
    private int arraySize;

    /**
     * This constructor takes array size and fills array in the form of butterfly.
     * @param arraySize
     */
    public Butterfly(int arraySize){
        this.arraySize = arraySize;
        arrayButterfly = new int[arraySize][arraySize];

        for (int i = 0; i < arraySize / 2 + 1; i++) {
            for (int j = 0; j < arraySize; j++) {
                if ((j < i) || (j >= (arraySize - i)))
                    arrayButterfly[i][j] = 0;
                else
                    arrayButterfly[i][j] = 1;
            }
        }

        for (int i = arraySize - 1; i >= arraySize / 2; i--) {
            for (int j = 0; j < arraySize; j++) {
                if ((j < (arraySize - 1 - i)) || (j > i))
                    arrayButterfly[i][j] = 0;
                else
                    arrayButterfly[i][j] = 1;
            }
        }
    }

    /**
     * This method prints array.
     */
    public void printButterfly(){
        for (int i = 0; i < arraySize; i++) {
            for (int j = 0; j < arraySize; j++) {
                System.out.print(arrayButterfly[i][j] + " ");
            }
            System.out.println();
        }
    }

}
