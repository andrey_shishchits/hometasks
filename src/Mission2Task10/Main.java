package Mission2Task10;

import Mission1Task4.UserInput;

/**
 * Created by User-PC on 29.04.2017.
 */
public class Main {
    public static void main(String[] args) {
        int arraySize = 0;

        arraySize = UserInput.inputInt("Enter array size: ");

        Butterfly butterfly = new Butterfly(arraySize);

        butterfly.printButterfly();
    }
}

