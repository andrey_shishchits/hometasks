package Mission2Task13;

import Mission1Task4.UserInput;
import java.util.Random;

/**
 * Created by User-PC on 04.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        Random rn = new Random();
        int arraySize = 0;

        arraySize = UserInput.inputInt("Enter array size: ");

        int[] array = new int[arraySize];

        for(int i = 0; i< arraySize; i++){
            array[i] = rn.nextInt();
        }

        array = MoreThanZero.leavePositivesOnly(array);

        for (int i = 0; i < array.length; i++){
            if(array[i] == 0) break;
            System.out.print(array[i] + " ");

        }
    }

}
