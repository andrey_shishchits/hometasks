package Mission2Task13;

/**
 * Created by User-PC on 04.05.2017.
 */

/**
 *This is class utility developed for converting input array into array with positive values only.
 */
public final class MoreThanZero {
    private MoreThanZero(){

    }

    /**
     * This method converts input array into positive only array.
     * @param array
     * @return int[]
     */
    public static int[] leavePositivesOnly(int[] array){
        int[] changedArray = new int[array.length];
        int changedArrayCounter = 0;

        for(int i = 0; i < array.length; i++){
            if(array[i] > 0) {
                changedArray[changedArrayCounter++] = array[i];
            }
        }

        return changedArray;
    }


}
