package Mission1Task1;


/**
 * Created by User-PC on 26.04.2017.
 */

import java.lang.Math;
import Mission1Task4.UserInput;
import Mission1Task4.View;

/**
 * There where no point to create a class 4 such a task
 */
public class Main {

    public static void main(String[] args) {

        int x = 0;

        x = UserInput.inputInt("Enter number: ");
        //честно стыренно из реализации abs=)
        x = (x < 0) ? -x : x;
        View.print("The absolute value of entered number is: " + x);
    }
}
