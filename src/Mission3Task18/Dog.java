package Mission3Task18;

/**
 * Created by User-PC on 13.05.2017.
 */

/**
 *This class demonstrates one of types of Singleton pattern.
 */
public class Dog {
    private static Dog instance;

    private Dog() {
    }

    /**
     * This method creates object of this class only once.
     * @return
     */
    public static Dog getInstance(){
        if(instance == null) instance = new Dog();
        return  instance;
    }
}
