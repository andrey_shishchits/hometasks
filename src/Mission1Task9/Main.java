package Mission1Task9;

import Mission1Task4.UserInput;

/**
 * Created by User-PC on 26.04.2017.
 */


public class Main {
        public static void main(String[] args) {

        int min;
        int max;

        min = (int) UserInput.inputNumber("Enter bottom limit: ");
        max = (int) UserInput.inputNumber("Enter top limit: ");

        HotAndCold game = new HotAndCold(min, max);

        game.findWinNumber();

    }

}
