package Mission1Task9;

import java.util.Random;

import Mission1Task4.UserInput;

import java.lang.Math;

/**
 * Created by User-PC on 26.04.2017.
 */

/**
 * Class emulates Hot & cold game.
 */
public class HotAndCold {

    private int winNumber;
    private int min;
    private int max;
    private int range;

    public HotAndCold(int first, int second) {
        min = Math.min(first, second);
        max = Math.max(first, second);
        range = max - min;

        Random rn = new Random();

        winNumber = rn.nextInt(range) + min;
    }

    /**
     * This method contains game logic.
     */
    public void findWinNumber() {
        int enteredNumber;

        do {
            enteredNumber = (int) UserInput.inputNumber("Enter number: ");
            if (enteredNumber < min || enteredNumber > max) {
                System.out.println("Too cold. Not even in range of this game!!! ");
                System.out.println("Enter number between: " + min + " and " + max);
                continue;
            }
            int currentRange = Math.abs(winNumber - enteredNumber);

            if (enteredNumber == winNumber) continue;
            if (range > currentRange) {
                range = currentRange;
                System.out.println("Warmer!");
            } else {
                range = currentRange;
                System.out.println("Colder!");
            }

        } while (winNumber != enteredNumber);

        System.out.println("Winner winner chicken dinner!!! ");

    }
}
