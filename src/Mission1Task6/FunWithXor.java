package Mission1Task6;

import Mission1Task4.UserInput;
import Mission1Task4.View;

/**
 * Created by User-PC on 26.04.2017.
 */

/**
 * This program switching values of fields by using XOR
 */
public class FunWithXor {
    public static void main(String[] args) {

        int firstNumber = 0;
        int secondNumber = 0;


        firstNumber = (int) UserInput.inputNumber("Enter first number: ");
        secondNumber = (int) UserInput.inputNumber("Enter second number: ");

        firstNumber = firstNumber ^ secondNumber;
        secondNumber = firstNumber ^ secondNumber;
        firstNumber = firstNumber ^ secondNumber;

        View.print("Currently first number: " + firstNumber);
        View.print("Currently second number: " + secondNumber);
    }
}
