package Mission3Task17;

import java.util.Comparator;

/**
 * Created by User-PC on 02.05.2017.
 */
public class NameComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Cat cat1 = (Cat) o1;
        Cat cat2 = (Cat) o2;

        return cat1.getName().compareTo(cat2.getName());
    }
}
