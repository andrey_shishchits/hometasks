package Mission3Task17;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by User-PC on 02.05.2017.
 */
public class Main {

    public static void main(String[] args) {

        Cat[] cats = new Cat[10];
        Comparator c = new NameComparator();

        cats[0] = new Cat("Vasya");
        cats[1] = new Cat("Igor");
        cats[2] = new Cat("Egor");
        cats[3] = new Cat("Misha");
        cats[4] = new Cat("Vitaliy");
        cats[5] = new Cat("Ekaterina");
        cats[6] = new Cat("Pesik");
        cats[7] = new Cat("Andrey");
        cats[8] = new Cat("Shalfei");
        cats[9] = new Cat("Kot");


        Arrays.sort(cats, c);

        for (int i = 0; i < 10; i++)
        {
            System.out.println(cats[i].toString());
        }

    }
}
