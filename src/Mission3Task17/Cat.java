package Mission3Task17;

/**
 * Created by User-PC on 02.05.2017.
 */

/**
 * This class contains information about cat.
 */
public class Cat {
    private String name;
    private int age;

    public Cat(String name) {
        this.name = name;
    }

    /**
     * This method serve to output information.
     * @return
     */
    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + age + '}';
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }




}
