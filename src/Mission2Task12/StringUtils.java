package Mission2Task12;

/**
 * Created by User-PC on 05.05.2017.
 */

/**
 * This is class utility developed for palindrome search.
 */
public final class StringUtils {

    private StringUtils() {
    }

    /**
     * Static method which looking for palindrome in string.
     *
     * @param str
     */
    public static void polindromFinder(String str) {
        String lettersOnly = str.replaceAll("[^А-Яа-яA-Za-z ]", "");
        String withoutExtraSpaces = lettersOnly.replaceAll("\\s+", " ");
        String toLower = withoutExtraSpaces.toLowerCase();
        String[] strArray = toLower.split(" ");

        for (String s : strArray) {
            int leftSide = 0;
            int rightaSide = s.length() - 1;
            char[] array = s.toCharArray();
            int counter = 0;

            while (leftSide <= rightaSide) {
                if (array[leftSide] != array[rightaSide]) break;
                counter++;
                leftSide++;
                rightaSide--;
            }

            if (counter >= s.length() / 2 & counter > 1) System.out.println(s);

        }
    }
}
