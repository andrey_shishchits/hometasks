package Mission1Task5;

/**
 * Created by User-PC on 26.04.2017.
 */

/**
 * This final class contains main ligic of programm.
 * Calculates nok & nod
 */
public final class NokAndNod {
    /**
     * This static method calculates nod.
     * Using Evclid algoritm.
     * @param a
     * @param b
     * @return
     */
    public final static int nod(int a, int b) {

        if (a < b) {
            int c = a;
            a = b;
            b = c;
        }

        int evclidQ;
        int evclidR;

        while (true) {
            evclidQ = (a - a % b) / b;
            evclidR = a - evclidQ * b;
            if (evclidR == 0) break;
            a = b;
            b = evclidR;
        }

        return b;

    }

    /**
     * Calculates nok.
     * Using connection of nok & nod
     * @param a
     * @param b
     * @return
     */
    public final static int nok(int a, int b) {
        return a * b / NokAndNod.nod(a, b);
    }
}
