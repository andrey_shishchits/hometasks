package Mission1Task5;

import Mission1Task4.UserInput;
import Mission1Task4.View;

/**
 * Created by User-PC on 26.04.2017.
 */

//НОК(a, b)=a·b:НОД(a, b)
public class Main {
    public static void main(String[] args) {

        int firstNumber = 0;
        int secondNumber = 0;

        firstNumber = (int) UserInput.inputNumber("Enter first number: ");
        secondNumber = (int) UserInput.inputNumber("Enter second number: ");

        View.print("NoD: " + NokAndNod.nod(firstNumber, secondNumber));
        View.print("NoK: " + NokAndNod.nok(firstNumber, secondNumber));


    }
}
