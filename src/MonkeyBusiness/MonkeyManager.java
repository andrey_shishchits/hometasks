package MonkeyBusiness;

/**
 * Created by User-PC on 16.05.2017.
 */
public class MonkeyManager {
    private MonkeyManager() {

    }

    public static int countForMe(Monkey monkey, Branch branch) {
        return monkey.countFruits(branch);
    }
}
