package MonkeyBusiness;

/**
 * Created by User-PC on 16.05.2017.
 */
public class UltraMonkey extends Monkey{
    @Override
    public int countFruits(Branch br) {
        fruitCounter += br.getBanana();
        fruitCounter +=br.getCoconut();

        if(br.getNextBranch() != null)
            for (Branch b: br.getNextBranch()) {
                countFruits(b);
            }
        return fruitCounter;
    }
}
