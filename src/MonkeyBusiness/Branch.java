package MonkeyBusiness;



/**
 * Created by User-PC on 06.05.2017.
 */
public class Branch {
    private int coconut;
    private int banana;
    private Branch[] nextBranch;

    public Branch(int coconut, int banana) {
        this.coconut = coconut;
        this.banana = banana;
        nextBranch = null;
      }

    public int getCoconut() {
        return coconut;
    }

    public int getBanana() {
        return banana;
    }

    public Branch[] getNextBranch() {
        return nextBranch;
    }

    public void setNextBranch(Branch[] nextBranch) {
        this.nextBranch = nextBranch;
    }
}
