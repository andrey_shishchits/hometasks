package MonkeyBusiness;

/**
 * Created by User-PC on 06.05.2017.
 */

public class Main {
    public static void main(String[] args) {
        int coconut = 2;
        int banana = 1;
        int branchAmount = 5;
        Branch b = new Branch(coconut, banana);
        Branch[] branches = new Branch[branchAmount];

        for (int i = 0; i < branchAmount; i++){
            branches[i] = new Branch(coconut, banana);
        }
        Branch[] branches1 = new Branch[branchAmount];
        for (int i = 0; i < branchAmount; i++)
        {
            branches1[i] = new Branch(coconut, banana);
        }
        for (Branch branch: branches) {
            branch.setNextBranch(branches1);

        }
        b.setNextBranch(branches);

        System.out.println("Coconut monkey: " + MonkeyManager.countForMe(new CocoMonkey(), b));
        System.out.println("Banana monkey: " + MonkeyManager.countForMe(new BananaMonkey(), b));
        System.out.println("Banana and coconut monkey: " + MonkeyManager.countForMe(new UltraMonkey(), b));

    }
}
